package br.edu.ufcg.virtus.zpl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.edu.ufcg.virtus.zpl.ZplPrinterApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ZplPrinterApplication.class)
public class ZplPrinterApplicationTests {

	@Test
	public void contextLoads() {
	}

}
