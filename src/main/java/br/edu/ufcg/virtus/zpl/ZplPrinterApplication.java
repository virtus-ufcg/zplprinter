package br.edu.ufcg.virtus.zpl;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * Esta classe tem as seguintes responsabilidade:
 * <ol>
 * <li>Habilitar a configuração automática do contexto Spring.</li>
 * <li>Mapear os pacotes onde estão definidos os componentes de serviço, repositório etc ... .</li>
 * <li>Startar a aplicação.</li>
 * <ol>
 *
 * @author Jefferson Araujo, jefferson.araujo[at]virtus[dot]ufcg[dot]edu[dot]br
 * @since 2016
 */
@SpringBootApplication
@ComponentScan("br.edu.ufcg.virtus.zpl")
public class ZplPrinterApplication {

}
