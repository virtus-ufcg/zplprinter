package br.edu.ufcg.virtus.zpl.printer;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

import fr.w3blog.zpl.constant.ZebraFont;
import fr.w3blog.zpl.model.ZebraLabel;
import fr.w3blog.zpl.model.ZebraPrintException;
import fr.w3blog.zpl.model.ZebraUtils;
import fr.w3blog.zpl.model.element.ZebraBarCode;
import fr.w3blog.zpl.model.element.ZebraBarCode39;
import fr.w3blog.zpl.model.element.ZebraText;

/**
 * Classe responsável por gerar arquivos ZPL para as etiquetas de identificação.
 *
 */
/**
 * Esta classe tem as seguintes responsabilidade:
 * <ol>
 * <li>Gerar arquivos ZPL para as etiquetas de identificação.</li>
 * <li>Imprimir etiquetas em impressora</li>
 * <ol>
 *
 * @author Jefferson Araujo, jefferson.araujo[at]virtus[dot]ufcg[dot]edu[dot]br
 * @since 2016
 */
public class ZplGenerator {

	public static String MO_FEEDER = "CB4";
	public static String MO_MACHINE = "20L";
	public static String MO_IDENTIFICATION_TYPE = "ST";
	public static String MATERIAL_PARTNUMBER = "P07R-4710U7-0040";
	public static String IDENTIFICATION_QUANTITY = "10000";
	public static String LPN = "000493616";
	public static String MO_CODE = "03781601001";
	public static String MATERIAL_DESCRIPTION = "C,SMD,0402,470PX,K(?0%),50V,CERAMIC,X7R,,,,,,,,REE";
	public static String USER = "JOÃO DA SILVA SANTOS";
	public static String PRINT_TIMESTAMP = "22/08/2016 22:10";

	public static final int LEFT_ELEMENT_X = 10;
	public static final int RIGHT_IDENTIFICATION_DATE_USER_Y = 280;
	public static final int RIGHT_IDENTIFICATION_MO_CODE_Y = 190;
	public static final int AXIS_Y_70 = 70;
	public static final int AXIS_Y_30 = 50;
	public static final int FONT_SIZE_8 = 8;
	public static final int FONT_SIZE_7 = 7;
	public static final int FONT_SIZE_6 = 6;
	public static final int FONT_SIZE_4 = 4;
	public static final int BAR_CODE_HEIGTH = 40;
	public static final int BAR_CODE_WIDTH = 3;
	public static final int BAR_CODE_RATIO = 2;
	public static final int WIDTH = 900;
	public static final int HEIGTH = 300;

	/**
	 * Construtor privado.
	 */
	private ZplGenerator() {
		// Checkstyle ...
	}

	/**
	 * Metodo de execução.
	 *
	 * @param args
	 *            argumentos.
	 * @throws NumberFormatException
	 * @throws ZebraPrintException
	 *             exceção caso não exista comunicação com a impressora.
	 */
	public static void main(final String[] args) {

		// Configuração da etiqueta
		final ZebraLabel zebraLabel = new ZebraLabel(ZplGenerator.WIDTH,
				ZplGenerator.HEIGTH);
		zebraLabel.setDefaultZebraFont(ZebraFont.ZEBRA_ZERO);

		// Informação de Máquina e Feeder
		zebraLabel.addElement(
				new ZebraText(LEFT_ELEMENT_X, AXIS_Y_30, ZplGenerator.MO_FEEDER, FONT_SIZE_8));
		zebraLabel.addElement(new ZebraText(140, AXIS_Y_30, "-", FONT_SIZE_8));

		// Informação de Feeder
		zebraLabel.addElement(new ZebraText(170, AXIS_Y_30, ZplGenerator.MO_IDENTIFICATION_TYPE, FONT_SIZE_8));
		// zebraLabel.addElement(new ZebraText(260, 60, "-", FONT_SIZE_8));

		// Identificador dos componentes que são montados primeiro no setup
		// zebraLabel.addElement(new ZebraText(155, AXIS_Y_30, MO_IDENTIFICATION_TYPE,
		// FONT_SIZE_8));

		// Código de barras do Partnumber
		final ZebraBarCode barCodePartnumber = new ZebraBarCode39(260, 60,
				"P" + ZplGenerator.MATERIAL_PARTNUMBER, BAR_CODE_HEIGTH, BAR_CODE_WIDTH,
				BAR_CODE_RATIO);
		barCodePartnumber.setShowTextInterpretation(true);
		barCodePartnumber.setShowTextInterpretationAbove(false);
		zebraLabel.addElement(barCodePartnumber);

		// Descrição do componente
		zebraLabel.addElement(
				new ZebraText(LEFT_ELEMENT_X, 140, ZplGenerator.MATERIAL_DESCRIPTION, FONT_SIZE_6));

		// Código de barras do LPN
		// final ZebraBarCode barCodeLpn = new ZebraBarCode39(LEFT_ELEMENT_X,
		// 165, MOIdentificationZplGenerator.LPN, BAR_CODE_HEIGTH,
		// BAR_CODE_WIDTH, BAR_CODE_RATIO);
		// barCodeLpn.setShowTextInterpretation(true);
		// barCodeLpn.setShowTextInterpretationAbove(false);
		// zebraLabel.addElement(barCodeLpn);

		// Identificação de data e hora
		zebraLabel.addElement(new ZebraText(LEFT_ELEMENT_X, RIGHT_IDENTIFICATION_DATE_USER_Y,
				ZplGenerator.PRINT_TIMESTAMP, FONT_SIZE_4));
		zebraLabel.addElement(new ZebraText(150, RIGHT_IDENTIFICATION_DATE_USER_Y, "-", FONT_SIZE_6));

		// Identificação de usuário
		zebraLabel.addElement(
				new ZebraText(180, RIGHT_IDENTIFICATION_DATE_USER_Y, ZplGenerator.USER, FONT_SIZE_4));

		// Linha
		// zebraLabel.addElement(new
		// ZebraNativeZpl("^FO10,245^GB430,0,1,b,0^FS^FX"));

		// Campos de preenchimento manual
		// zebraLabel.addElement(new ZebraText(LEFT_ELEMENT_X, 260, "ID:",
		// FONT_SIZE_4));
		// zebraLabel.addElement(new ZebraText(300, 260, "DEV:", FONT_SIZE_4));

		// Número da Manufacturing Order
		zebraLabel.addElement(new ZebraText(510, RIGHT_IDENTIFICATION_MO_CODE_Y, "MO:", FONT_SIZE_7));
		zebraLabel.addElement(
				new ZebraText(565, RIGHT_IDENTIFICATION_MO_CODE_Y, ZplGenerator.MO_CODE, FONT_SIZE_7));

		// Código de barras da quantidade
		final ZebraBarCode barCodeQuantidade = new ZebraBarCode39(510, 240,
				"Q" + ZplGenerator.IDENTIFICATION_QUANTITY, BAR_CODE_HEIGTH, BAR_CODE_WIDTH,
				BAR_CODE_RATIO);
		barCodeQuantidade.setShowTextInterpretation(true);
		barCodeQuantidade.setShowTextInterpretationAbove(false);
		zebraLabel.addElement(barCodeQuantidade);

		// String hostname = "0";
		// String zplCode = "0";
		// int port = 0;

		Scanner entrada = new Scanner(System.in);

		System.out.println("Digite o IP da Impressora (Ex: 127.0.0.1)");
		String hostname = entrada.next();

		System.out.println("Digite a porta da Impressora (Ex: 9100)");
		int port = entrada.nextInt();

		System.out.println(
				"Digite a nome do arquivo (Ex: C://ZPL-identification.zpl ou digite 0 para imprimir o template)");
		String zplCode = entrada.next();

		try {

			if (hostname.equals("0")) {
				hostname = "127.0.0.1";
			}

			if (port == 0) {
				port = 9100;
			}

			if (zplCode.equals("0")) {
				zplCode = zebraLabel.getZplCode();
			} else {
				 populateParams();
				zplCode = readFile(zplCode);
			}

			ZebraUtils.printZpl(zplCode, hostname, Integer.valueOf(port));
			System.out.printf("Impressão realizada com sucesso " + "\n");
			System.out.println(zplCode);
		} catch (ZebraPrintException e) {
			System.err.printf("Erro ao imprimir eqtiqueta", e);
			e.printStackTrace();
		} catch (IOException e) {
			System.err.printf("Erro ao ler arquivo ZPL", e);
			e.printStackTrace();
		}
		entrada.close();
	}

	/**
	 * Método responsável por popular os parametros que o sistema externo monta.
	 */
	private static void populateParams() {
		MO_FEEDER = "${MO_FEEDER}";
		MO_MACHINE = "${MO_MACHINE}";
		MO_IDENTIFICATION_TYPE = "${MO_IDENTIFICATION_TYPE}";
		MATERIAL_PARTNUMBER = "${MATERIAL_PARTNUMBER}";
		IDENTIFICATION_QUANTITY = "${IDENTIFICATION_QUANTITY}";
		LPN = "${LPN}";
		MO_CODE = "${MO_CODE}";
		MATERIAL_DESCRIPTION = "${MATERIAL_DESCRIPTION}";
		USER = "${USER}";
		PRINT_TIMESTAMP = "${PRINT_TIMESTAMP}";
	}

	/**
	 * Metodo responsável por ler um arquivo.
	 * 
	 * @param filename
	 *            nome do arquivo.
	 * @return texto do arquivo.
	 * @throws IOException
	 *             exceção de arquivo inválido.
	 */
	public static String readFile(String filename) throws IOException {
		String content = null;
		File file = new File(filename);
		FileReader reader = null;
		try {
			reader = new FileReader(file);
			char[] chars = new char[(int) file.length()];
			reader.read(chars);
			content = new String(chars);
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				reader.close();
			}
		}
		return content;
	}

}
